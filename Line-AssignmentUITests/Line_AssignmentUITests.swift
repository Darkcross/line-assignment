//
//  Line_AssignmentUITests.swift
//  Line-AssignmentUITests
//
//  Created by admin on 6/20/19.
//  Copyright © 2019 M. All rights reserved.
//

import XCTest

class Line_AssignmentUITests: XCTestCase {

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.

        // In UI tests it is usually best to stop immediately when a failure occurs.
        continueAfterFailure = false

        // UI tests must launch the application that they test. Doing this in setup will make sure it happens for each test method.
        XCUIApplication().launch()

        // In UI tests it’s important to set the initial state - such as interface orientation - required for your tests before they run. The setUp method is a good place to do this.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }
    
    // Test create Post and update data in main table
    func testCreatePost()
    {
        let app = XCUIApplication()
        app.navigationBars["Timeline"].buttons["Compose"].tap()
        let count = app.tables.cells.count
        app.textViews["titleTextView"].tap()
        app.textViews["titleTextView"].typeText("Test Data")
        
        app.navigationBars["New Post"].buttons["Create"].tap()
        
        let newCount = app.tables.cells.count
        print("testCreatePost (count,newCount)",count,newCount)
        XCTAssert( newCount == count + 1 )
    }

}
