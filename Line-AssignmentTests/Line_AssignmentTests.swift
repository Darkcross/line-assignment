//
//  Line_AssignmentTests.swift
//  Line-AssignmentTests
//
//  Created by admin on 6/20/19.
//  Copyright © 2019 M. All rights reserved.
//

import XCTest
import RealmSwift
@testable import Line_Assignment

class Line_AssignmentTests: XCTestCase {

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }
    
    // App use Realm to store image's path
    // Image in Table count should match image in Documents folder
    func testImageFolder()
    {
        let fm = FileManager.default
        do
        {
            let imageFiles = try fm.contentsOfDirectory(atPath: Config.fullImageParhDir)
            let realm = try Realm()
            let allImage = realm.objects( PostImage.self )
            
            print("imageFiles (file,table)",imageFiles.count ,allImage.count)
            XCTAssert(imageFiles.count == allImage.count, "Table count should match image in Documents folder.")
        }
        catch
        {
            XCTAssert(false, error as! String)
        }
    }


    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }

}
