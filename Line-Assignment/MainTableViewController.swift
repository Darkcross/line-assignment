//
//  MainTableViewController.swift
//  Line-Assignment
//
//  Created by Darkcross on 21/6/2562 BE.
//  Copyright © 2562 M. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class MainTableViewController: UITableViewController,UINavigationControllerDelegate {

    var mainTableViewModel = MainTableViewModel.instance()
    let disposeBag = DisposeBag()
//    var disposeAble:Disposable?
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.delegate = self
        // Do any additional setup after loading the view, typically from a nib.
        initSearchBar()
        bindingModel()
        mainTableViewModel.fetchData()
        
//        self.navigationItem.searchController = UISearchController(searchResultsController: nil)
    }
    
    func initSearchBar()
    {
        let searchController = UISearchController(searchResultsController: nil)
        searchController.obscuresBackgroundDuringPresentation = false
        definesPresentationContext = true
        navigationItem.searchController = searchController
    }
    
    func bindingModel()  {
        tableView.register( UINib(nibName: "TitleViewCell", bundle: nil) , forCellReuseIdentifier: "TitleViewCell")

        tableView.dataSource = nil
        mainTableViewModel.tableData.bind(to: tableView.rx.items(cellIdentifier: "TitleViewCell")) {
            (index, cellModel, cell) in
            let titleCell = cell as? TitleViewCell
            titleCell?.setModel(titleCellModel: cellModel)
        }
        .disposed(by: disposeBag)
        navigationItem.searchController?.searchBar.rx.text.bind(onNext: mainTableViewModel.inputSearch ).disposed(by: disposeBag)
        
    }
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let cellmodel =  mainTableViewModel.tableData.value[ indexPath.row ]
        return cellmodel.estimateCellHeight;
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return 10
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TitleViewCell") ?? CellTool.loadNib("TitleViewCell")[0] as! UITableViewCell
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cellmodel =  mainTableViewModel.tableData.value[ indexPath.row ]
        PostViewModel.instance().setupModel(model: cellmodel)
        performSegue(withIdentifier: "detail", sender: nil)
    }
    
    
    @IBAction func onTestButton()
    {
//        print("ax",mainTableViewModel.tableData)
//        mainTableViewModel.fetchData2()
    }
    
    func navigationController(_ navigationController: UINavigationController, willShow viewController: UIViewController, animated: Bool) {
//        (viewController as? MainMenu)?.loadData()
//        print("XSS")
    }
}
