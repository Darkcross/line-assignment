//
//  post.swift
//  Line-Assignment
//
//  Created by Darkcross on 21/6/2562 BE.
//  Copyright © 2562 M. All rights reserved.
//

import RealmSwift

class PostData: Object {
    @objc dynamic var title: String?
    @objc dynamic var date: Date? = Date()
    let images = List<PostImage>()
    
    
}

class PostImage:Object{
    @objc dynamic var imageUrl: String?
    var image:UIImage?
    let owners = LinkingObjects(fromType: PostData.self, property: "images")
}
