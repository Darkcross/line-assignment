//
//  ImageDetailViewController.swift
//  Line-Assignment
//
//  Created by Darkcross on 23/6/2562 BE.
//  Copyright © 2562 M. All rights reserved.
//

import Foundation
import UIKit
import RxSwift
import RxCocoa

class ImageDetailViewController: UIViewController,UIScrollViewDelegate {
    
    @IBOutlet var imageView:UIImageView!
    @IBOutlet var scrollView:UIScrollView!
    var viewModel:ImageDetailViewModel = ImageDetailViewModel.instance()
    let disposeBag = DisposeBag()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        scrollView.contentSize = imageView.frame.size
        scrollView.minimumZoomScale = 1
        scrollView.maximumZoomScale = 3
        scrollView.zoomScale = scrollView.minimumZoomScale
        
        bindingModel()
    }
    
    func bindingModel()  {
        viewModel.showImage.bind(to: imageView.rx.image).disposed(by: disposeBag)
    }
    
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return imageView
    }
    
}
