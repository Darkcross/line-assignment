//
//  PostCell.swift
//  Line-Assignment
//
//  Created by Darkcross on 23/6/2562 BE.
//  Copyright © 2562 M. All rights reserved.
//

import UIKit
import RxSwift

class PostViewCell: TitleViewCell {
    @IBOutlet var showImageView:UIImageView?
    
    func setModel ( cellModel:PostCellModel )
    {
        if disposeBag != nil
        {
            //clear disposeBag
            disposeBag = nil
        }
        
        disposeBag = DisposeBag()
        switch cellModel.cellId {
        case .textCell:
            cellModel.titleText.bind(to: self.titleLabel.rx.text ).disposed(by: disposeBag!)
            cellModel.dateText.bind(to: dateLabel.rx.text  ).disposed(by: disposeBag!)
            break
        case .imageCell:
            cellModel.imageViewModels.bind(onNext: bindImage ).disposed(by: disposeBag!)
            break
        }
    }
    
    func bindImage (image:ImageViewModel?)
    {
        if image != nil && showImageView != nil
        {
            image!.image.bind(to: showImageView!.rx.image ).disposed(by: disposeBag!)
            image?.loadImage()
        }
    }
}
