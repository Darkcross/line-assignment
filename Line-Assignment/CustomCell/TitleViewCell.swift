//
//  TitleViewCell.swift
//  Line-Assignment
//
//  Created by Darkcross on 21/6/2562 BE.
//  Copyright © 2562 M. All rights reserved.
//

import UIKit
import RxSwift
class TitleViewCell :UITableViewCell {
    @IBOutlet var titleLabel:UILabel!
    @IBOutlet var dateLabel:UILabel!
    @IBOutlet var moreLabel:UILabel!
    var extraview:UIView?
    var imageField:UIView = UIView(frame: CGRect(x: 10, y: 0, width: TableCellDefine.cellWidth-20, height: TableCellDefine.extendImageHeight)  )
    var imageViews:[UIImageView]?
    weak var currentModel:TitleCellModel?
    var disposeBag:DisposeBag?
    
    
    func setModel ( titleCellModel:TitleCellModel )
    {
        currentModel = titleCellModel
        if disposeBag != nil
        {
            //clear disposeBag
            disposeBag = nil
        }
        //Bind new Model
        disposeBag = DisposeBag()
        currentModel?.titleText.bind(onNext: self.titleLabelRx ).disposed(by: disposeBag!)
        currentModel?.dateText.bind(to: dateLabel.rx.text  ).disposed(by: disposeBag!)
        currentModel?.imageViewModels.bind(onNext: self.bindImageField ).disposed(by: disposeBag!)
    }
    
    func titleLabelRx (str:String?)  {
        moreLabel.removeFromSuperview()
        titleLabel.text = str
        
        var titleHeight = currentModel?.estimateTextHeight ?? TableCellDefine.cellHeight
        if currentModel!.isTextHeightOver
        {
            titleHeight -= 12
            moreLabel.setAnchorPoint( CGPoint(x: 1,y: 0) )
            moreLabel.center = CGPoint(x:  titleLabel.frame.size.width , y:  titleLabel.frame.size.height )
            titleLabel.addSubview(moreLabel)
        }
        titleLabel.frame = CGRect(x: 10, y: 20, width: TableCellDefine.cellWidth - 20, height: titleHeight)
        
        
    }
    
    func bindImageField( imageViewModel:[ImageViewModel]? )
    {
        guard let models = imageViewModel else {
            imageField.removeFromSuperview()
            return
        }
        
        let titleHeight = currentModel!.estimateTextHeight + 20
        imageField.setAnchorPoint(CGPoint(x: 0, y: 0))
        imageField.center = CGPoint(x: titleLabel.frame.origin.x, y: titleHeight)
        contentView.addSubview( imageField )
        
        imageField.subviews.forEach{ $0.removeFromSuperview() }
        let frames = getImageFormation(imageCount: models.count)

        
        for (index,imageModel) in models.enumerated()
        {
//            let width = imageField.frame.size.width/3.0
            let imageView = AdvImageView(frame: frames[index] )
            imageView.frame =  imageView.frame.insetBy(dx: 2, dy: 2)
            imageView.backgroundColor = UIColor.gray
            imageView.contentMode = .scaleAspectFill
            imageView.clipsToBounds = true
            imageView.setupIndicator()
            imageModel.image.bind(to: imageView.rx.image ).disposed(by: disposeBag!)
            imageModel.loadImage()
            imageField.addSubview(imageView)
        }
    }
    
    func getImageFormation( imageCount:Int) -> [CGRect]
    {
        let bound = imageField.frame.size
        var result:[CGRect] = []
        switch imageCount {
        case 1:
            let rect = CGRect(origin: CGPoint.zero, size: bound)
            result.append(rect)
            break
        case 2:
            for i in 0...2
            {
                let rect = CGRect(x: CGFloat( i )  * (bound.width/2)  , y: 0, width: bound.width/2, height: bound.height)
                result.append(rect)
            }
            break
        case 3:
            var rect = CGRect(x: 0 , y: 0, width: bound.width/2, height: bound.height)
            result.append(rect)
            rect = CGRect(x:  bound.width/2 , y: 0, width: bound.width/2, height: bound.height/2)
            result.append(rect)
            rect = CGRect(x:  bound.width/2 , y: bound.height/2, width: bound.width/2, height: bound.height/2)
            result.append(rect)
            break
        default:
            break
        }
        return result
    }
}
