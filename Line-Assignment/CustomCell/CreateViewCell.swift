//
//  CreateViewCell.swift
//  Line-Assignment
//
//  Created by Darkcross on 23/6/2562 BE.
//  Copyright © 2562 M. All rights reserved.
//

import UIKit
import RxSwift

class CreateViewCell: UITableViewCell {
    @IBOutlet var textView:UITextView!
    @IBOutlet var imageField:UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        textView.layer.cornerRadius = 10
    }
    
}

