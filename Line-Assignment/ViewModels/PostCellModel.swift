//
//  PostViewCellModel.swift
//  Line-Assignment
//
//  Created by Darkcross on 23/6/2562 BE.
//  Copyright © 2562 M. All rights reserved.
//

import Foundation
import UIKit
import RxCocoa

class PostCellModel {
    
    enum celltype:String {
        case textCell
        case imageCell
    }
    enum celltypeIndex: Int {
        case textCell = 0
        case imageCell
    }
    
    var estimateCellHeight:CGFloat = TableCellDefine.cellHeight
    var titleText = BehaviorRelay<String?>(value: nil)
    var dateText = BehaviorRelay<String?>(value: nil)
    var imageViewModels = BehaviorRelay< ImageViewModel? >(value: nil)
    var cellId:celltype = .textCell
    var cellIdIndex:celltypeIndex = .textCell
    
    func setupWithTitle(text:String)
    {
        self.cellId = .textCell
        self.cellIdIndex = .textCell
        self.titleText.accept(text)
        self.estimateCellHeight = text.height(withConstrainedWidth:  TableCellDefine.cellWidth - 20 , font: TableCellDefine.cellFont ) + 10
        self.estimateCellHeight = max( self.estimateCellHeight,TableCellDefine.cellHeight + 20)
        
    }
    
    func setupWithImage( image:ImageViewModel ) {
        self.cellId = .imageCell
        self.cellIdIndex = .imageCell
        self.imageViewModels.accept(image)
        self.estimateCellHeight = TableCellDefine.extendImageHeight
    }
}
