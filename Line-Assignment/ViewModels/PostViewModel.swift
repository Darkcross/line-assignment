//
//  MainTableViewModel.swift
//  Line-Assignment
//
//  Created by Darkcross on 21/6/2562 BE.
//  Copyright © 2562 M. All rights reserved.
//

import Foundation
import RealmSwift
import RxCocoa
import RxSwift

private var instance_postViewModel:PostViewModel?
class PostViewModel
{
    
//    var titleText: BehaviorRelay< String? >
    
    var tableDataSources = BehaviorRelay< [PostCellModel]? >(value: nil)
    
    static func instance() -> PostViewModel {
        if instance_postViewModel == nil
        {
            instance_postViewModel = PostViewModel()
        }
        return instance_postViewModel!
    }
    
    func setupModel( model:TitleCellModel )
    {
        var newSource = [PostCellModel]()
        if let titleText = model.titleText.value {
            let cellModel = PostCellModel()
            cellModel.setupWithTitle(text: titleText)
            cellModel.dateText.accept( model.dateText.value )
            newSource.append(cellModel)
        }
        
        if let images = model.imageViewModels.value {
            for image in images
            {
                 let cellModel = PostCellModel()
                cellModel.setupWithImage(image: image)
                newSource.append(cellModel)
            }
        }
        self.tableDataSources.accept(newSource)
    }
    
}
