//
//  CreateViewModel.swift
//  Line-Assignment
//
//  Created by Darkcross on 23/6/2562 BE.
//  Copyright © 2562 M. All rights reserved.
//

import Foundation
import UIKit
import RxCocoa
import RealmSwift

private var instance_createViewModel:CreateViewModel?

class CreateViewModel {
    
    var tableDataSources = BehaviorRelay< [CreateViewCell]? >(value: nil)
    var dataModel = BehaviorRelay< ImageViewModel? >(value: nil)
    var titleString:String?
    var images:[PostImage?]?
//    var postData:PostData?
    
//    var postString = BehaviorRelay< String? >(value: nil)
    
    static func instance() -> CreateViewModel {
        if instance_createViewModel == nil
        {
            instance_createViewModel = CreateViewModel()
        }
        return instance_createViewModel!
    }
    
    
    func initPostDataModel()
    {
        setDataSources()
        titleString = ""
        images = [PostImage?]()
        for _ in 0...2 {
            images?.append( PostImage() )
        }
    }
    
    func setDataSources()
    {
        let newCell = CellTool.loadNib("CreateCell")[0] as! CreateViewCell
        tableDataSources.accept([newCell])
    }
    
    func commitSave()
    {
        guard titleString != nil && images != nil else {
            fatalError( "No commit data" )
        }
        
        let postData = PostData()
        postData.title = self.titleString?.trimmingCharacters(in: CharacterSet.whitespaces)
        
        for image in images!
        {
            guard image?.imageUrl != nil else { continue }
            Config.saveImage(imageModel: image!)
            postData.images.append(image!)
        }
        
        let realm = try! Realm()
        try! realm.write {
            realm.add(postData)
        }
    }

}
