//
//  ImageDetailViewModel.swift
//  Line-Assignment
//
//  Created by Darkcross on 23/6/2562 BE.
//  Copyright © 2562 M. All rights reserved.
//

import Foundation
import UIKit
import RxCocoa
import RxSwift

private var instance_imageDetailViewModel:ImageDetailViewModel?

class ImageDetailViewModel {
    var showImage = BehaviorRelay<UIImage?>(value: nil)
    var disposeBag:DisposeBag?
    
    static func instance() -> ImageDetailViewModel {
        if instance_imageDetailViewModel == nil
        {
            instance_imageDetailViewModel = ImageDetailViewModel()
        }
        return instance_imageDetailViewModel!
    }
    
    
    func setModel(model:ImageViewModel)  {
        if disposeBag != nil { disposeBag = nil  }
        disposeBag = DisposeBag()
        model.image.bind(to: showImage).disposed(by: disposeBag!)
        model.loadImage()
    }
}
