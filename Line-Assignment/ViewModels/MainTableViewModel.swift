//
//  MainTableViewModel.swift
//  Line-Assignment
//
//  Created by Darkcross on 21/6/2562 BE.
//  Copyright © 2562 M. All rights reserved.
//

import Foundation
import RealmSwift
import RxCocoa
import RxSwift

private var instance_mainTableViewModel:MainTableViewModel?
class MainTableViewModel
{
    var tableData = BehaviorRelay< [TitleCellModel] >(value: [] )
    private var rawTableData:[TitleCellModel]?
    private var searchString:String?
    
    static func instance() -> MainTableViewModel {
        if instance_mainTableViewModel == nil
        {
            instance_mainTableViewModel = MainTableViewModel()
        }
        return instance_mainTableViewModel!
    }
    
    func fetchData()
    {
        
        let realm = try! Realm()
        let posts = realm.objects(PostData.self).sorted(byKeyPath: "date", ascending: false)
        let tableData = try! posts.map { (postData) throws -> TitleCellModel in
            return TitleCellModel(postData: postData)
        }
        self.rawTableData = tableData
        self.update()
    }
    
    func inputSearch (str:String?)
    {
        searchString = str
        self.update()
    }
    
    func searchRawData(with str:String?) -> [TitleCellModel] {
        guard var searchStr = str else {
            return self.rawTableData ?? []
        }
        guard searchStr.count != 0 else
        {
            return self.rawTableData ?? []
        }
        searchStr = searchStr.trimmingCharacters(in: CharacterSet.whitespaces)
        let result = self.rawTableData?.filter({ (cellModule) -> Bool in
            return cellModule.titleText.value?.contains(searchStr) ?? false
        })
        return result ?? []
    }
    
    func update()
    {
        let result = self.searchRawData(with: searchString)
        self.tableData.accept(result)
    }
}
