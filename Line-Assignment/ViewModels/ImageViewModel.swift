//
//  ImageViewModel.swift
//  Line-Assignment
//
//  Created by Darkcross on 22/6/2562 BE.
//  Copyright © 2562 M. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class ImageViewModel {
    var image = BehaviorRelay<UIImage?>(value: nil)
    var onLoading = false
    var imageUrl:String?
    private var disposeBag = DisposeBag()
    
    init( imageUrl:String ) {
        self.imageUrl = imageUrl
    }
    
    func loadImage()  {
        
//        print("start",onLoading,self.image.value)
        guard onLoading == false && self.image.value == nil else {
            return
        }
        guard let imgUrl = self.imageUrl else {
            return
        }
        
        onLoading = true
       let url = URL(fileURLWithPath: Config.documentsDir!).appendingPathComponent(imgUrl)
        URLSession.shared.dataTask(with: url, completionHandler: { (data, response, error) in
            //print("RESPONSE FROM API: \(response)")
            if error != nil {
                return
            }
            DispatchQueue.main.async {
                if let data = data {
                    if let downloadedImage = UIImage(data: data) {
                        self.image.accept( downloadedImage )
                        self.onLoading = false
                    }
                }
            }
        }).resume()
    }
    
}
