//
//  TitleCellModule.swift
//  Line-Assignment
//
//  Created by Darkcross on 21/6/2562 BE.
//  Copyright © 2562 M. All rights reserved.
//

import UIKit
import RxCocoa

struct TableCellDefine {
    static let cellFont:UIFont = {  UIFont.systemFont(ofSize: 17) }()
    static let cellWidth:CGFloat =  { UIScreen.main.bounds.size.width }()
    static let cellHeight:CGFloat =  60
    static let extendImageHeight:CGFloat =  220
    static let maxTextHeight:CGFloat = 70
}

class TitleCellModel {
    var titleText = BehaviorRelay<String?>(value: nil)
    var dateText = BehaviorRelay<String?>(value: nil)
    var imageViewModels = BehaviorRelay< [ImageViewModel]? >(value: nil)
    var estimateTextHeight:CGFloat = TableCellDefine.cellHeight
    var estimateCellHeight:CGFloat = TableCellDefine.cellHeight
    var isTextHeightOver = false
    
    init ( postData:PostData )
    {
        //Setup titleText
        self.estimateCellHeight = 30
        setupDate(date: postData.date)
        setupTitle(text: postData.title)
        let images = try! postData.images.map { (postImage) throws -> String?  in
            return postImage.imageUrl
        }
        setupImage(imageUrls: images)
        self.estimateCellHeight = max(self.estimateCellHeight, TableCellDefine.cellHeight)
    }
    func setupTitle(text:String?)
    {
        if let title = text
        {
            self.titleText.accept(title)
            self.estimateTextHeight = title.height(withConstrainedWidth:  TableCellDefine.cellWidth - 20 , font: TableCellDefine.cellFont ) + 10
            if estimateTextHeight > TableCellDefine.maxTextHeight
            {
                estimateTextHeight = TableCellDefine.maxTextHeight + 12
                isTextHeightOver = true
            }
        }
        else
        {
            self.estimateTextHeight = TableCellDefine.cellHeight
        }
        self.estimateCellHeight += self.estimateTextHeight
    }
    func setupDate(date:Date?)
    {
        if let currentDate = date
        {
            let calendar = Calendar.current
            let formatter = DateFormatter()
            formatter.dateFormat = calendar.isDateInToday(currentDate) ? "HH:mm a" : "yyyy-MM-dd"
            dateText.accept( formatter.string(from: currentDate) + " ▸" )
        }
    }
    func setupImage(imageUrls:[String?]?)
    {
        guard let images = imageUrls else {
            return
        }
        guard images.count != 0 else {
            return
        }
        var imageViewModels:[ImageViewModel]  = []
        for image in images
        {
            guard let url = image else
            {
                continue
            }
            imageViewModels.append( ImageViewModel( imageUrl: url ) )
        }
        self.imageViewModels.accept(imageViewModels)
        self.estimateCellHeight += TableCellDefine.extendImageHeight
    }
}
