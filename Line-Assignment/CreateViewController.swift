//
//  CreateViewController.swift
//  Line-Assignment
//
//  Created by Darkcross on 21/6/2562 BE.
//  Copyright © 2562 M. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class CreateViewController: UITableViewController,UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    let viewModel = CreateViewModel.instance()
    let disposeBag = DisposeBag()
    var tableDataSources:[CreateViewCell]?
    var selectImageIndex:Int?
    //    var disposeAble:Disposable?
    override func viewDidLoad() {
        super.viewDidLoad()
        bindingModel()
    }
    
    func bindingModel()  {
        viewModel.initPostDataModel()
        viewModel.tableDataSources.bind(onNext: setTableDataSources  ).disposed(by: disposeBag)
    }
    
    func setTableDataSources( sources:[CreateViewCell]? )
    {
        self.tableDataSources = sources
        if let cell = tableDataSources?[0]{
            cell.textView.rx.text.bind { [weak self] (str) in
                self?.viewModel.titleString = str
            }.disposed(by: disposeBag)
            for imageButton in cell.imageField.subviews
            {
                if let button:EditImageButton = imageButton as? EditImageButton
                {
                    button.callBack = { [unowned self] (isEdit,index) in
                        self.uploadImage(isEdit, index)
                    }
                }
            }
        }
        tableView.reloadData()
    }
    func setImageToButton(index:Int,image:UIImage?)
    {
        if let cell = tableDataSources?[0]{
            let button = cell.imageField.viewWithTag(index) as! EditImageButton
            button.imageView.image = image
        }
    }
    
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return tableDataSources![indexPath.row].contentView.frame.size.height
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tableDataSources?.count ?? 0
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return tableDataSources![indexPath.row]
    }
    
    
    func uploadImage(_ isEdit:Bool,_ index:Int)  {
        selectImageIndex = index
        let alertController = UIAlertController(title: "Add Image", message: "", preferredStyle: .actionSheet)
        
        var action = UIAlertAction(title: "Camera", style: .default) { [unowned self] (action) in
            self.showImagePicker(sourceType: .camera)
        }
        alertController.addAction(action)
        action = UIAlertAction(title: "Storage", style: .default) { [unowned self] (action) in
            self.showImagePicker(sourceType: .photoLibrary)
        }
        alertController.addAction(action)
        action = UIAlertAction(title: "Cancel", style: .cancel) { (action) in
        }
        alertController.addAction(action)
        present(alertController, animated: true, completion: nil)
        
    }
    
    func showImagePicker( sourceType:UIImagePickerController.SourceType )
    {
        #if targetEnvironment(simulator)
        if sourceType == .camera
        {
            AdvAlert.show(title: "Error", msg: "Simulator have no camera.", viewController: self)
            return
        }
        #endif
        
        let imagePicker = UIImagePickerController()
        imagePicker.sourceType = sourceType
        imagePicker.delegate = self
        present(imagePicker, animated: true, completion: nil);
    }
    
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        //Get image from UIImagePickerController
        print ("info",info)
        guard let imageURL = info[UIImagePickerController.InfoKey.imageURL] as? URL else {
            AdvAlert.show(title: "Error", msg: "Can't read this url", viewController: self)
            return
        }
        guard let selectedImage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage else {
            AdvAlert.show(title: "Error", msg: "Can't read this image.", viewController: self)
            return
        }
        
        
        if let index = selectImageIndex
        {
            let postImage = viewModel.images?[index-1]
            postImage?.image = selectedImage
            postImage?.imageUrl = NSString(string: Config.imagePathDir).appendingPathComponent(imageURL.lastPathComponent)
            self.setImageToButton(index: index, image: selectedImage)
        }
        
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func onCreate()
    {
        viewModel.commitSave()
        MainTableViewModel.instance().fetchData()
        self.navigationController?.popViewController(animated: true)
    }
    
}

