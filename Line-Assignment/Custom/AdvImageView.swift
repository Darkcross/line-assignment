//
//  AdvImageView.swift
//  Line-Assignment
//
//  Created by Darkcross on 22/6/2562 BE.
//  Copyright © 2562 M. All rights reserved.
//
import Foundation
import UIKit
import RxSwift

class AdvImageView: UIImageView {
    private var indicator:UIActivityIndicatorView?
    var disposeBag = DisposeBag()
    var disposeAble:Disposable?
    
    func setupIndicator()  {
        if self.indicator == nil {
            self.indicator = UIActivityIndicatorView(style: UIActivityIndicatorView.Style.whiteLarge)
            self.indicator!.hidesWhenStopped = true
            self.addSubview(self.indicator!)
        }
        self.image = nil
        self.indicator!.center =  CGPoint(x: self.bounds.size.width/2 , y: self.bounds.size.height/2 )
        self.indicator!.startAnimating()
    }
    
    
    override var image: UIImage! {
        get {
            return super.image
        }
        set {
            super.image = newValue
            guard let loadView = self.indicator else {return}
            loadView.stopAnimating()
        }
    }
    
    func bindModel( model:ImageViewModel )  {
        self.disposeAble?.dispose()
        self.disposeAble = model.image.bind(to: self.rx.image)
        self.disposeAble?.disposed(by: disposeBag)
        model.loadImage()
    }
    
}

