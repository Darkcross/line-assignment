//
//  AdvAlert.swift
//  FaceCameraSwift
//
//  Created by Darkcross on 4/3/2561 BE.
//  Copyright © 2561 M. All rights reserved.
//

import Foundation
import UIKit

class AdvAlert {
    static func show(title:String,msg:String,viewController:UIViewController)
    {
        let alert = UIAlertController(title: title, message: msg, preferredStyle: UIAlertController.Style.alert)
        let action = UIAlertAction(title: "Ok", style: UIAlertAction.Style.default) { (Action) in
        }
        alert.addAction(action)
        viewController.present(alert, animated: true, completion: nil)
    }
}
