//
//  EditImageButton.swift
//  FaceCameraSwift
//
//  Created by Darkcross on 3/3/2561 BE.
//  Copyright © 2561 M. All rights reserved.
//

import Foundation
import UIKit

class EditImageButton: UIView {
    @IBOutlet var contentView:UIView!
    @IBOutlet var button:UIButton!
    @IBOutlet var imageView:UIImageView!
    var isEdit:Bool = false
    var edit:Bool
    {
        set{
            isEdit = newValue
            changeEdit()
        }
        get
        {
            return isEdit
        }
    }
    var index:Int = -1;
    var callBack:( (_ isEdit:Bool,_ index:Int) -> Void )?
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        Bundle.main.loadNibNamed("EditImageButton", owner: self, options: nil)
        contentView.frame.size = self.frame.size
        self.addSubview(contentView)
        self.edit = true
        self.index = self.tag
    }
    
    override func didAddSubview(_ subview: UIView) {
    }
    
    func changeEdit()
    {
        if isEdit {
            button.setImage( UIImage(named: "edit.png") , for: .normal)
            imageView.alpha = 0.5
        }
        else
        {
            button.setImage( nil , for: .normal)
            imageView.alpha = 1
        }
    }
    
    @IBAction func onButton(_ button:Any)
    {
        callBack?( self.isEdit, self.index  )
    }
}
