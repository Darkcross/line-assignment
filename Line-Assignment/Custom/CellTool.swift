//
//  NibTool.swift
//  FaceCameraSwift
//
//  Created by admin on 2/12/18.
//  Copyright © 2018 M. All rights reserved.
//

import Foundation
import UIKit
class CellTool {
//    Load all view from nib
    static func loadNib(_ str:String) -> [Any]
    {
        return Bundle.main.loadNibNamed(str, owner: nil, options: nil)!
    }
}

