//
//  Config.swift
//  Line-Assignment
//
//  Created by Darkcross on 21/6/2562 BE.
//  Copyright © 2562 M. All rights reserved.
//

import RealmSwift
class Config {
    static let documentsDir:String? = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).first
    static let imagePathDir:String = "images"
    static let fullImageParhDir =  NSString(string: documentsDir!).appendingPathComponent(imagePathDir)
    static func initDb() {
        do
        {
            let realm = try Realm()
//            print("realm \(realm.configuration.fileURL)")
            //Check database
            let posts = realm.objects(PostData.self)
            if posts.isEmpty
            {
                // Fill test data
                try createImageDir()
                try fillTestDb()
            }
        }
        catch
        {
            print(error)
            fatalError("Can't init Database")
        }
    }
    
    private static func fillTestDb() throws
     {
        let realm = try Realm()
        let imageList = realm.objects(PostImage.self)
        let fixImage = [
                         [],
                         [imageList[0]] ,
                         [imageList[2],imageList[3],imageList[1]],
                         [imageList[1],imageList[3]]
        ]
        
        let list:List<PostData> = List<PostData>()
        for index in 1...20
        {
            let post = PostData()
            post.title = "Test Data " + String(index)
            for img in fixImage[ (index-1)%fixImage.count ]
            {
                 post.images.append(img)
            }
            list.append(post)
        }
        
        list[4].title = "Bangkok, Thailand’s capital, is a large city known for ornate shrines and vibrant street life. The boat-filled Chao Phraya River feeds its network of canals, flowing past the Rattanakosin royal district, home to opulent Grand Palace and its sacred Wat Phra Kaew Temple. Nearby is Wat Pho Temple with an enormous reclining Buddha and, on the opposite shore, Wat Arun Temple with its steep steps and Khmer-style spire."
        
        list[9].title = "DescriptionLondon, the capital of England and the United Kingdom, is a 21st-century city with history stretching back to Roman times. At its centre stand the imposing Houses of Parliament, the iconic ‘Big Ben’ clock tower and Westminster Abbey, site of British monarch coronations. Across the Thames River, the London Eye observation wheel provides panoramic views of the South Bank cultural complex, and the entire city."
        
        try realm.write {
            realm.add(list)
        }
    }
    
    private static func createImageDir() throws
    {
        let imageDir:String = fullImageParhDir
        let fm = FileManager.default
        if !fm.fileExists(atPath: imageDir)
        {
            do
            {
                try fm.createDirectory(atPath: imageDir, withIntermediateDirectories: true, attributes: nil)
           
                let assetsPath = Bundle.main.paths(forResourcesOfType: nil, inDirectory: "Assets2")
                let imageDb = List<PostImage>()
                for aPath in assetsPath
                {
                    let fileName = NSString(string: aPath).lastPathComponent
                    let newFileName =  NSString(string: imageDir).appendingPathComponent(fileName)
                    try fm.copyItem(atPath: aPath, toPath:newFileName)
                    imageDb.append( PostImage(value: ["imageUrl":  NSString(string: imagePathDir).appendingPathComponent(fileName)  ] ) )
                }
                let realm = try Realm()
                try realm.write {
                    realm.add(imageDb)
                }
            }
            catch
            {
                throw DbError.cantCreateDB
            }
        }
    }
    
    static func saveImage(imageModel:PostImage)
    {
        guard imageModel.imageUrl != nil && documentsDir != nil else {
            return
        }
        let path = URL(fileURLWithPath: documentsDir!).appendingPathComponent(imageModel.imageUrl!)
        try! imageModel.image?.pngData()?.write(to: path)
    }
    
    enum DbError: Error {
        case cantCreateDB
    }
}
