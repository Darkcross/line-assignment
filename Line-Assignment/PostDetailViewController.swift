//
//  PostDetailViewController.swift
//  Line-Assignment
//
//  Created by Darkcross on 21/6/2562 BE.
//  Copyright © 2562 M. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class PostDetailViewController: UITableViewController {
    
//    var tableDataSource:[Post]
    var viewModel:PostViewModel = PostViewModel.instance()
    let disposeBag = DisposeBag()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        bindingModel()
    }
    
    func bindingModel()  {
        viewModel.tableDataSources.bind(onNext: setTableDataSources  ).disposed(by: disposeBag)
    }
    
    func setTableDataSources( sources:[PostCellModel]? )
    {
        tableView.reloadData()
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let cellModel = viewModel.tableDataSources.value![indexPath.row]
        return cellModel.estimateCellHeight;
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.tableDataSources.value!.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellModel = viewModel.tableDataSources.value![indexPath.row]
        var cell = tableView.dequeueReusableCell(withIdentifier: cellModel.cellId.rawValue) as? PostViewCell
        if cell == nil
        {
            cell = CellTool.loadNib( "PostCell" )[cellModel.cellIdIndex.rawValue] as? PostViewCell
        }
        cell!.setModel(cellModel: cellModel)
        return cell!
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cellModel = viewModel.tableDataSources.value![indexPath.row]
        if cellModel.cellId == .imageCell
        {
            ImageDetailViewModel.instance().setModel(model: cellModel.imageViewModels.value!)
            performSegue(withIdentifier: "image", sender: nil)
        }
    }
    
    
    @IBAction func onCloseButton ()
    {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func onDotButton ()
    {
        AdvAlert.show(title: "Alert", msg: "No Function", viewController: self)
    }
}
